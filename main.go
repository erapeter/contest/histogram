package main

import (
	"context"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/erapeter/contest/histogram/parser"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

var (
	defaultPoolSize = 10
)

func main() {
	var (
		dir      string
		poolSize = defaultPoolSize
		err      error
	)

	switch len(os.Args) {
	case 2:
		dir = os.Args[1]
	case 3:
		dir = os.Args[1]

		poolSize, err = strconv.Atoi(os.Args[2])
		if err != nil {
			log.Println(err)
			return
		}
	default:
		log.Println("usage args: [directory] string, [pool size] int")
		return
	}

	dp := &parser.DirParser{Path: dir, PoolSize: poolSize}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	h, err := dp.GetCount(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	if err := createHistogram(h); err != nil {
		log.Println(err)
		return
	}

}

func createHistogram(h map[string]int) (err error) {
	p, err := plot.New()
	if err != nil {
		return
	}

	vals := plotter.Values{}

	names := make([]string, 0, len(h))

	w := vg.Points(20)

	for k, v := range h {
		vals = append(vals, float64(v))
		names = append(names, k)
	}

	bars, err := plotter.NewBarChart(vals, w)
	if err != nil {
		return
	}

	p.Add(bars)
	p.NominalX(names...)
	if err := p.Save(10*vg.Inch, 10*vg.Inch, "hist.jpeg"); err != nil {
		return err
	}

	return
}
