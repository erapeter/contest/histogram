package parser

import (
	"context"
	"reflect"
	"testing"
)

func TestGetCount(t *testing.T) {

	wantHist := map[string]int{"!": 3, ",": 3, ".": 3, "<": 3, ">": 3, "A": 3, "B": 3, "C": 3, "H": 6, "I": 3, "a": 6, "b": 6, "c": 6, "j": 9}

	dp := &DirParser{Path: "test_dir/", PoolSize: 3}

	hist, err := dp.GetCount(context.TODO())
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if !reflect.DeepEqual(hist, wantHist) {
		t.Errorf(
			"expected histogram %#v,\n result - %#v",
			wantHist, hist,
		)
	}

}
