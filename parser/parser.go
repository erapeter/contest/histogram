package parser

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
)

// DirParser - объект для парса директории
type DirParser struct {
	Path     string
	PoolSize int
}

// GetCount - подсчёт количества символов в файлах дирректории
func (d *DirParser) GetCount(ctx context.Context) (h map[string]int, err error) {

	h = make(map[string]int)

	readerStream := make(chan []byte)
	errorStream := make(chan error)

	go d.parseDir(ctx, readerStream, errorStream)

readerLoop:
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case line, ok := <-readerStream:
			if !ok {
				break readerLoop
			}
			for _, l := range line {

				// пропуск пробелов
				if l == ' ' {
					continue
				}
				h[string(l)]++
			}
		case err = <-errorStream:
			if err != nil {
				break readerLoop
			}
		}
	}

	return
}

func (d *DirParser) parseDir(
	ctx context.Context,
	readerStream chan<- []byte,
	errorStream chan<- error,
) {
	defer close(errorStream)
	defer close(readerStream)

	pool := make(chan struct{}, d.PoolSize)

	files, err := ioutil.ReadDir(d.Path)
	if err != nil {
		errorStream <- err
		return
	}

	wg := new(sync.WaitGroup)

	for _, f := range files {
		if f.IsDir() {
			continue
		}

		wg.Add(1)

		pool <- struct{}{}

		go func(file os.FileInfo) {
			defer func() {
				wg.Done()
				<-pool
			}()

			err := scanFile(d.Path+file.Name(), readerStream)
			if err != nil {
				errorStream <- err
				return
			}

		}(f)

	}
	wg.Wait()
}

func scanFile(
	filepath string,
	readerStream chan<- []byte,
) (err error) {

	f, err := os.Open(filepath)
	if err != nil {
		return
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := bytes.Trim(scanner.Bytes(), "\n")
		if len(line) == 0 {
			continue
		}

		readerStream <- line
	}

	errScanner := scanner.Err()
	if errScanner != nil {
		err = fmt.Errorf("scanner: %w", errScanner)
		return
	}

	return
}
